#!/usr/bin/env bash
set -ex

curl https://dist.crystal-lang.org/apt/setup.sh | bash
apt-get update -y
apt-get install -y crystal
