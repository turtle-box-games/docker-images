Docker Build Images
===================

Docker images for various things.

Packer
------

Built off of the Docker image and adds [Packer](https://www.packer.io/).
This allows for easy building of other Docker images.
All other images in this repository are built using Packer and use this container for CI builds.

