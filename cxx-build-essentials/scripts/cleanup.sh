#!/usr/bin/env bash
set -ex

# This script cleans up unnecessary files to reduce final image size.

# Clear out apt cache.
apt-get clean -y
rm -rf /var/lib/apt/lists/*

# Remove temporary files.
rm -rf /tmp/* /var/tmp/*
