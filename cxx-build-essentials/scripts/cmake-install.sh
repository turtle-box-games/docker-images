#!/usr/bin/env bash
set -ex

readonly version_major=3
readonly version_minor=11
readonly version_revision=3
readonly short_version="${version_major}.${version_minor}"
readonly long_version="${short_version}.${version_revision}"

# Download CMake self-extractor script and run it.
pushd /tmp
curl --silent --show-error --fail https://cmake.org/files/v${short_version}/cmake-${long_version}-Linux-x86_64.sh > cmake-install.sh
chmod o+x cmake-install.sh
./cmake-install.sh --skip-license --prefix=/usr/local
popd
