#!/usr/bin/env bash
set -ex

apt-get update -y
apt-get install -y \
  apt-transport-https \
  build-essential \
  pkg-config \
  doxygen \
  graphviz \
  curl \
  git \
  libx11-dev \
  libxrandr-dev \
  libxinerama-dev \
  libxcursor-dev \
  mesa-common-dev \
  xvfb
